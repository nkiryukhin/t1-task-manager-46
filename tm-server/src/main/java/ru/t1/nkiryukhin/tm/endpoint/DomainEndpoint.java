package ru.t1.nkiryukhin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.api.endpoint.IDomainEndpoint;
import ru.t1.nkiryukhin.tm.api.service.IDomainService;
import ru.t1.nkiryukhin.tm.api.service.IServiceLocator;
import ru.t1.nkiryukhin.tm.dto.request.DatabaseCreateRequest;
import ru.t1.nkiryukhin.tm.dto.request.DatabaseDropRequest;
import ru.t1.nkiryukhin.tm.dto.response.DatabaseCreateResponse;
import ru.t1.nkiryukhin.tm.dto.response.DatabaseDropResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;


@WebService(endpointInterface = "ru.t1.nkiryukhin.tm.api.endpoint.IDomainEndpoint")
public class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    private IDomainService getDomainService() {
        return this.getServiceLocator().getDomainService();
    }

    @NotNull
    @Override
    @WebMethod
    public DatabaseDropResponse dropDatabase(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DatabaseDropRequest request
    ) {
        String passphrase = request.getPassphrase();
        getDomainService().dropDatabase(passphrase);
        return new DatabaseDropResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DatabaseCreateResponse createDatabase(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DatabaseCreateRequest request
    ) {
        getDomainService().createDatabase();
        return new DatabaseCreateResponse();
    }

}